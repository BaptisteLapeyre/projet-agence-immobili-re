package agenceImmo;

public abstract class Personne {

	private String nom;
	private String adresse;
	private String tel;
	private String email;
	private Voeux desir;
	
	public Personne(String nom, String adresse, String tel, String email) {
		super();
		this.nom = nom;
		this.adresse = adresse;
		this.tel = tel;
		this.email = email;
	}
	
	public void desir(String type, String localisation, int prix, int surface, int piece) {
		this.desir = new Voeux(type, localisation, prix, surface, piece);
	}

	@Override
	public String toString() {
		return "nom : " + nom + ", adresse : " + adresse + ", tel : " + tel + ", email : " + email;
	}

	public String getNom() {
		return nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public String getTel() {
		return tel;
	}

	public String getEmail() {
		return email;
	}

	public Voeux getDesir() {
		return desir;
	}
	
	
}
