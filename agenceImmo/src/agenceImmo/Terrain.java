package agenceImmo;

public class Terrain extends BienImmobilier{
	
	private int surfaceSol;
	private int longueurFacade;

	public Terrain(Personne personne, int prixDemande, String orientation, String adresse, String dateSouhaitee, String duree, int surfaceSol, int longueurFacade) {
		super(personne, prixDemande, orientation, adresse, dateSouhaitee, duree);
		this.surfaceSol = surfaceSol;
		this.longueurFacade = longueurFacade;
		
	}

}
