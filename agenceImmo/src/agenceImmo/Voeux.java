package agenceImmo;

public class Voeux {


	private String localisation;
	private int prix;
	private int surface;
	private int piece;
	private String type;
	
	public Voeux(String type, String localisation, int prix, int surface, int piece) {
		super();
		this.type = type;
		this.localisation = localisation;
		this.prix = prix;
		this.surface = surface;
		this.piece = piece;
	}
	
	
	public int getPiece() {
		return piece;
	}
	

	public String getType() {
		return type;
	}


	public String getLocalisation() {
		return localisation;
	}


	public int getPrix() {
		return prix;
	}


	public int getSurface() {
		return surface;
	}

	
}