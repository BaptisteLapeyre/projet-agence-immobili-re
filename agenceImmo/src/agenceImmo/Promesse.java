package agenceImmo;

public class Promesse {
	private int prixVendeur;
	private String addrNotaire;
	private String dateVente;
	private int commission;
	private int frais;
	private BienImmobilier bien;
	
	public Promesse(int prixVendeur, String addrNotaire, String dateVente, int frais, BienImmobilier bien) {
		super();
		this.prixVendeur = prixVendeur;
		this.addrNotaire = addrNotaire;
		this.dateVente = dateVente;
		this.frais = frais;
		this.bien = bien;
		this.commission = (int)(this.prixVendeur*7/100);
	}
	
	public int getPrixAchat() {
		return this.commission + this.prixVendeur + this.frais;
	}
	
	public int getCotion() {
		return (int)(this.prixVendeur/10);
	}

	public int getCommission() {
		return commission;
	}

	public int getPrixVendeur() {
		return prixVendeur;
	}

	public String getAddrNotaire() {
		return addrNotaire;
	}

	public String getDateVente() {
		return dateVente;
	}

	public int getFrais() {
		return frais;
	}

	public BienImmobilier getBien() {
		return bien;
	}
}
