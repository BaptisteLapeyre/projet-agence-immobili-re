package agenceImmo;

public class Description {
	private Docs document;

	private BienImmobilier bien;

	public Description(Media media, BienImmobilier bien) {
		
		this.bien = bien;
		if(media == Media.internet) {
			this.document = new Web();
		}else if (media == Media.journaux) {
			this.document = new journaux();
		}else {
			this.document = new Presse();
		}
		
		
	}
}
