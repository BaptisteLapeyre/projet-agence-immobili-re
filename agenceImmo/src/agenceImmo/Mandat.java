package agenceImmo;

public class Mandat {

	private String dateSouhaitee;
	private int prix;
	private String duree;
	private Personne personne;
	
	public Mandat(String dateSouhaitee, int prix, String duree, Personne personne) {
		super();
		this.dateSouhaitee = dateSouhaitee;
		this.prix = prix;
		this.duree = duree;
		this.personne = personne;
	}

	public int getPrix() {
		return prix;
	}

	public Personne getPersonne() {
		return personne;
	}

	public String getDateSouhaitee() {
		return dateSouhaitee;
	}

	public String getDuree() {
		return duree;
	}
	
	
	
	
}
