package agenceImmo;

import java.util.ArrayList;
import java.util.Scanner;


public class Agence {
	private ArrayList<Annonce> uneAnnonce;
	private ArrayList<BienImmobilier> bien1;
	private ArrayList<BienImmobilier> bienVendu;
	private ArrayList<RendezVous> rdv;
	private ArrayList<Personne> clients;
	private ArrayList<Promesse> promesse;
	
	public Agence() {
		this.uneAnnonce = new ArrayList<Annonce>();
		this.bien1 = new ArrayList<BienImmobilier>();
		this.rdv = new ArrayList<RendezVous>();
		this.clients = new ArrayList<Personne>();
		this.promesse = new ArrayList<Promesse>();
		this.bienVendu = new ArrayList<BienImmobilier>();
	}
	
	public ArrayList<Annonce> getUneAnnonce() {
		return uneAnnonce;
	}

	public ArrayList<BienImmobilier> getBien1() {
		return bien1;
	}

	public ArrayList<BienImmobilier> getBienVendu() {
		return bienVendu;
	}

	public ArrayList<RendezVous> getRdv() {
		return rdv;
	}

	public ArrayList<Personne> getClients() {
		return clients;
	}

	public ArrayList<Promesse> getPromesse() {
		return promesse;
	}

	public void newClient (boolean physique, String nom, String adresse, String tel, String email, String juridique, String siren) {
		Personne client;
		
		if (physique) {
			client = new Physique(nom, adresse, tel, email);
		}else {
			client = new Moral(nom, adresse, tel, email, juridique, siren);
		}
		this.clients.add(client);
		
	}
	
	public void newRDV (String date, Personne vendeur, Personne acheteur) {
		RendezVous rdv = new RendezVous(date, vendeur, acheteur);
		this.rdv.add(rdv);
	}
	
	public void newRDVvente (String date, Personne vendeur) { //mise en vente d'un nouveau bien
		this.newRDV(date, vendeur, null);
	}
	
	public void newAchat (String date, Personne vendeur, Personne acheteur, BienImmobilier bien, int prixVendeur, String addrNotaire, String dateVente, int frais) { //signature promesse de vente
		this.newRDV(date, vendeur, acheteur);
		this.promesse.add(new Promesse(prixVendeur, addrNotaire, dateVente, frais, bien));
		this.bienVendu.add(bien);
		this.bien1.remove(bien);
	}
	
	public void addBien (BienImmobilier bien) {
		this.bien1.add(bien);
	}
	
	public void newPub(Media media, BienImmobilier bien) {
		Annonce annonce = new Annonce(media, bien);
		this.uneAnnonce.add(annonce);
	}
	
	public void venteRealise(BienImmobilier bien) {
		this.bien1.remove(bien);
		this.bienVendu.add(bien);
	}
	
	public String afficheClients() {
		if (this.clients.isEmpty()) {
			return "aucun client";
		}
		String s = "liste des clients :";
		int i = 0;
		for (Personne client : this.clients) {
			s += "\nn°" + i + " : [" + client.toString() + "]";
			i++;
		}
		return s;
	}
	
	public String afficheBiens() {
		if (this.bien1.isEmpty()) {
			return "aucun bien";
		}
		String s = "liste des biens :";
		int i = 0;
		for (BienImmobilier bien : this.bien1) {
			s += "\nn°" + i + " : [" + bien.toString() + "]";
			i++;
		}
		return s;
	}
	
	 
	@Override
	public String toString() {
		String s = "il y a " + this.bien1.size() + " bien a vendre, pour une moyenne de ";
		int prix = 0;
		for (BienImmobilier bien : this.bien1) {
			prix += bien.getPrix();
		}
		if ( this.bien1.size() > 0) {prix = (int)(prix / this.bien1.size());}
		s += prix + "€\nil y a " + this.promesse.size() + " bien vendu, pour une moyenne de ";
		int prixVendu = 0;
		int commission = 0;
		for (Promesse promesse : this.promesse) {
			prixVendu += promesse.getPrixAchat();
			commission += promesse.getCommission();
		}
		if ( this.promesse.size() > 0) {prixVendu = (int)(prixVendu / this.promesse.size());}
		s += prixVendu + "€, rapportant " + commission + "€ a l'agence";
		return s;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Agence agence = new Agence();
		
		//preremplissage
		agence.newClient(true, "jean", "2 rue jean michel", "0123456789", "jean@michel.fr", null, null);
		agence.addBien(new Terrain(agence.clients.get(0), 120000, "sud", "4 rue jean miche", "12/04/2021", "6 jours", 250, 35));
		
		agence.newClient(true, "albert", "4 rue jean michel", "0123456788", "albert@michel.fr", null, null);
		agence.addBien(new Terrain(agence.clients.get(1), 160000, "sud", "8 rue jean miche", "12/04/2021", "12 jours", 270, 35));
		
		agence.newAchat("15/05/2021", agence.bien1.get(0).getPersonne(), agence.clients.get(1), agence.bien1.get(0), 140000, "2 rue jean michel", "15/05/2021", 5000);
		agence.newAchat("16/05/2021", agence.bien1.get(0).getPersonne(), agence.clients.get(0), agence.bien1.get(0), 140000, "2 rue jean michel", "16/05/2021", 5000);
		
		
		int choix = 0;
		while (choix != 7) {
			String menu = "selectionnez une action parmit :"
					+ "\n 1 : creer un nouveau client"
					+ "\n 2 : creer un nouveau rendez vous pour une nouvelle vente"
					+ "\n 3 : creer un nouveau rendez vous pour un nouvel achat"
					+ "\n 4 : creer un nouveau bien immobilier"
					+ "\n 5 : creer une nouvele pub"
					+ "\n 6 : edité statistice"
					+ "\n 7 : quitter";
			System.out.println(menu);
			choix = scanner.nextInt();
			switch (choix) {
			case 1: {
				boolean physique;
				String nom = null, adresse = null, tel = null, email = null, juridique = null, siren = null;
				while(true) {
					System.out.println("est ce une personne physique? 'oui' ou 'non'");
					scanner.nextLine();
					String type = scanner.nextLine();
					if (type.equals("oui")) {
						physique = true;
						
						System.out.println("saisir le nom de la personne : ");
						nom = scanner.nextLine();
						
						System.out.println("saisir l'adresse de la personne : ");
						adresse = scanner.nextLine();
						
						System.out.println("saisir le telephone de la personne : ");
						tel = scanner.nextLine();
						
						System.out.println("saisir l'email de la personne : ");
						email = scanner.nextLine();
						
						break;
					} else if (type.equals("non")) {
						physique = false;
						
						System.out.println("saisir le nom de l'entreprise : ");
						nom = scanner.nextLine();
						
						System.out.println("saisir l'adresse de l'entreprise : ");
						adresse = scanner.nextLine();
						
						System.out.println("saisir le telephone de l'entreprise : ");
						tel = scanner.nextLine();
						
						System.out.println("saisir l'email de l'entreprise : ");
						email = scanner.nextLine();
						
						System.out.println("saisir la forme jusridique de l'entreprise : ");
						juridique = scanner.nextLine();
						
						System.out.println("saisir le nulero SIREN de l'entreprise : ");
						siren = scanner.nextLine();
						
						break;
					} else {
						System.out.println("reponse non reconnue");
					}
				}
				agence.newClient(physique, nom, adresse, tel, email, juridique, siren);
				System.out.println("personne creer");
				break;
			}
			case 2: {
				scanner.nextLine();
				System.out.println("saisir la date du rendez vous : ");
				String date = scanner.nextLine();
				
				System.out.println(agence.afficheClients());
				System.out.println("saisir le numero du client parmis la liste ci-dessus : ");
				int num = scanner.nextInt();
				
				agence.newRDVvente(date, agence.clients.get(num));
				break;
			}
			case 3: {
				scanner.nextLine();
				System.out.println("saisir la date du rendez vous : ");
				String date = scanner.nextLine();
				
				System.out.println(agence.afficheClients());
				System.out.println("saisir le numero de l'acheteur parmis la liste ci-dessus : ");
				int acheteur = scanner.nextInt();
				
				System.out.println(agence.afficheBiens());
				System.out.println("saisir le numero de du bien parmis la liste ci-dessus : ");
				int bien = scanner.nextInt();
				
				System.out.println("saisir le prix de vente du vendeur : ");
				int prixVendeur = scanner.nextInt();
				
				scanner.nextLine();
				System.out.println("saisir l'adresse du notaire : ");
				String addrNotaire = scanner.nextLine();
				
				System.out.println("saisir la date de la vente : ");
				String dateVente = scanner.nextLine();
				
				System.out.println("saisir les frais : ");
				int frais = scanner.nextInt();
				
				agence.newAchat(date, agence.bien1.get(bien).getPersonne(), agence.clients.get(acheteur), agence.bien1.get(bien), prixVendeur, addrNotaire, dateVente, frais);
				break;
			}
			case 4: {
				BienImmobilier bien = null;
				int type = 0;
				while(type != 1 && type != 2 && type != 3) {
					System.out.println("selectionnez le type de bien parmis : "
							+ "\n 1 : terrain"
							+ "\n 2 : maison"
							+ "\n 3 : appartment");
					scanner.nextLine();
					type = scanner.nextInt();
				}
				System.out.println(agence.afficheClients());
				System.out.println("saisir le numero du vendeur parmis la liste ci-dessus : ");
				int vendeur = scanner.nextInt();
				
				System.out.println("saisir le prix de vente : ");
				int prix = scanner.nextInt();
				
				scanner.nextLine();
				System.out.println("saisir l'orientation du bien : ");
				String orientation = scanner.nextLine();
				
				System.out.println("saisir l'adresse du bien : ");
				String adresse = scanner.nextLine();
				
				System.out.println("saisir la date de vente souhaite : ");
				String dateSouhaitee = scanner.nextLine();
				
				System.out.println("saisir la duree du mandat : ");
				String duree = scanner.nextLine();
				
				switch (type) {
				case 1: {
					System.out.println("saisir la surface du bien : ");
					int surfaceSol = scanner.nextInt();
					
					System.out.println("saisir la longueur de la facade : ");
					int longueurFacade = scanner.nextInt();
					
					bien = new Terrain(agence.clients.get(vendeur), prix, orientation, adresse, dateSouhaitee, duree, surfaceSol, longueurFacade);
					break;
				}
				case 2: {
					System.out.println("saisir la surface du bien : ");
					int surfaceHabitalbe = scanner.nextInt();
					
					System.out.println("saisir le nombre de pieces : ");
					int nombreDepieces = scanner.nextInt();
					
					System.out.println("saisir le nombre d'etages : ");
					int nombreEtages = scanner.nextInt();
					
					scanner.nextLine();
					System.out.println("saisir le type de chauffage : ");
					String typeDeChauffage = scanner.nextLine();
					
					bien = new Maison(agence.clients.get(vendeur), prix, orientation, adresse, dateSouhaitee, duree, surfaceHabitalbe, nombreDepieces, nombreEtages, typeDeChauffage);
					break;
				}
				case 3: {
					System.out.println("saisir le nombre de pieces : ");
					int nombreDepieces = scanner.nextInt();
					
					System.out.println("saisir le nombre d'etages : ");
					int nombreEtages = scanner.nextInt();
					
					System.out.println("saisir les charges mensuelles : ");
					int chargesMensuelles = scanner.nextInt();
					
					bien = new Appart(agence.clients.get(vendeur), prix, orientation, adresse, dateSouhaitee, duree, nombreDepieces, nombreEtages, chargesMensuelles);
					break;
				}
				default:
					System.out.println("type non reconnu");
				}
				
				agence.addBien(bien);
				break;
			}
			case 5: {
				System.out.println(agence.afficheBiens());
				System.out.println("saisir le numero de du bien parmis la liste ci-dessus : ");
				int bien = scanner.nextInt();
				
				Media med = null;
				int type = 0;
				while(type != 1 && type != 2 && type != 3) {
					System.out.println("selectionnez le type de media parmis : "
							+ "\n 1 : internet"
							+ "\n 2 : presse"
							+ "\n 3 : journaux");
					scanner.nextLine();
					type = scanner.nextInt();
					
					switch (type) {
					case 1: {
						med = Media.internet;
						break;
					}
					case 2: {
						med = Media.presse;
						break;
					}
					case 3: {
						med = Media.journaux;
						break;
					}
					default:
						System.out.println("type non reconnu");
						break;
					}
				}
				
				agence.newPub(med, agence.bien1.get(bien));
				break;
			}
			case 6: {
				System.out.println(agence);
			}
			default:
				break;
			}
		} 
	}

}


