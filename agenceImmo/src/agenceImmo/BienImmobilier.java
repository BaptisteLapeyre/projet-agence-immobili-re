package agenceImmo;

public abstract class BienImmobilier {
	private int id;
	private String orientation;
	private String adresse;
	public enum Type {TERRAIN, APPART, MAISON};
	private static int idCounter = 0;
	private Mandat mandat;
	
	public BienImmobilier(Personne personne,int prixDemande, String orientation, String adresse, String dateSouhaitee, String duree) {
		this.id = idCounter++;
		this.orientation = orientation;
		this.adresse = adresse;
		this.mandat = new Mandat(dateSouhaitee, prixDemande, duree, personne);
	}

	@Override
	public String toString() {
		return "id : " + id + ", prixDemande : " + this.mandat.getPrix() + ", orientation : " + orientation
				+ ", adresse : " + adresse + ", dateDeVente : " + this.mandat.getDateSouhaitee();
	}

	public Personne getPersonne() {
		return this.mandat.getPersonne();
	}
	
	public int getPrix() {
		return this.mandat.getPrix();
	}
	
}
