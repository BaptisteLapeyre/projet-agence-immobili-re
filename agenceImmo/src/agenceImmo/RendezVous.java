package agenceImmo;

public class RendezVous {
	private String date;
	private Personne vendeur;
	private Personne acheteur;
	private Mandat mandat;
	
	public RendezVous(String date, Personne vendeur, Personne acheteur) {
		super();
		this.date = date;
		this.vendeur = vendeur;
		this.acheteur = acheteur;
	}
	
	public void newMandat(String dateSouhaitee, int prix, String duree) {
		mandat = new Mandat(dateSouhaitee, prix, duree, this.vendeur);
	}
	
	public BienImmobilier newBien(BienImmobilier.Type type, int id, Personne personne, int prixDemande, String orientation, String adresse, String dateSouhaitee, String duree,int nombreDepieces, int nombreEtages, int chargesMensuelles, int surfaceHabitalbe, String typeDeChauffage, int surfaceSol, int longueurFacade) {
		BienImmobilier bien = null;
		if(type == BienImmobilier.Type.APPART) {
			bien = new Appart(personne, prixDemande, orientation, adresse, dateSouhaitee, duree, nombreDepieces, nombreEtages, chargesMensuelles);
		}
		if(type == BienImmobilier.Type.MAISON) {
			bien = new Maison(personne, prixDemande, orientation, adresse, dateSouhaitee, duree, surfaceHabitalbe, nombreDepieces, nombreEtages, typeDeChauffage);
		}
		if(type == BienImmobilier.Type.TERRAIN) {
			bien = new Terrain(personne, prixDemande, orientation, adresse, dateSouhaitee, duree, surfaceSol, longueurFacade);
		} 
		return bien;
	}

	public String getDate() {
		return date;
	}

	public Personne getVendeur() {
		return vendeur;
	}

	public Personne getAcheteur() {
		return acheteur;
	}

	public Mandat getMandat() {
		return mandat;
	}

	
	
}
