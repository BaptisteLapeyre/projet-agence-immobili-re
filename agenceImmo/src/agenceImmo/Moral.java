package agenceImmo;

public class Moral extends Personne {
	private String juridique;
	private String siren;

	public Moral(String nom, String adresse, String tel, String email, String juridique, String siren) {
		super(nom, adresse, tel, email);
		this.juridique = juridique;
		this.siren = siren;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString() + " juridique : " + this.juridique + " SIREN : " + this.siren;
	}
	
	

	
}
