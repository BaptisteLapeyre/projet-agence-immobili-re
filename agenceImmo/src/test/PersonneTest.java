package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import agenceImmo.Personne;
import agenceImmo.Physique;
import agenceImmo.Voeux;

public class PersonneTest {
	private Personne personne;
	
	@Before
	public void setUp() throws Exception {
		personne = new Physique("Homer","Springfield", "118218", "homerjsimpson@mail.com");
	}

	@Test
	public void testPersonne() {
		assertNotNull("Linstance n'est pas cr�e", personne);
	}
	
	@Test
	public void testGetNom(){
		assertEquals("Le nom est incorrect", "Homer", personne.getNom());
	}
	
	@Test
	public void testGetAdresse(){
		assertEquals("L'adresse est incorrect", "Springfield", personne.getAdresse());
	}
	
	@Test
	public void testGetTel(){
		assertEquals("Le tel est incorrect", "118218", personne.getTel());
	}
	
	@Test
	public void testGetEmail(){
		assertEquals("L'email est incorrect", "homerjsimpson@mail.com", personne.getEmail());
	}
	
	@Test
	public void testDesir() {
		Voeux desir = new Voeux("t1", "springfield", 3500, 50, 5);
		assertEquals("Le type incorrect", "t1", desir.getType());
		assertEquals("La localisaton incorrect", "springfield", desir.getLocalisation());
		assertEquals("Le prix incorrect", 3500, desir.getPrix());
		assertEquals("La surface incorrect", 50 , desir.getSurface());
		assertEquals("Le nbr de piece incorrect", 5, desir.getPiece());
	}

}
