package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import agenceImmo.BienImmobilier;
import agenceImmo.Maison;
import agenceImmo.Personne;
import agenceImmo.Physique;
import agenceImmo.Promesse;

public class TestPromesse {
	Personne personne;
	BienImmobilier maison;
	
	Promesse promesse;
	@Before
	public void setUp() throws Exception {
		personne = new Physique("Homer","Springfield", "118218", "homerjsimpson@mail.com");
		maison = new Maison(personne, 3500, "S", "Springfield", "2019/02/09", "1", 50, 5, 2, "Electrique");
		
		promesse = new Promesse(3500, "Springfield", "2019/02/09", 1000, maison);
	}

	@Test
	public void testPromesse() {
		assertNotNull("Linstance n'est pas cr�e", promesse);
		
	}
	
	@Test
	public void testPrixAchat() {
		int com = (int)(3500*7/100);
		int prixAchat = com + 3500 + 1000;
		assertEquals("Le prix d'achat est incorrect", prixAchat, promesse.getPrixAchat());
		}
	
	@Test
	public void testCotion() {
		int cotion = (int)(3500/10);
		assertEquals("La cotion est incorrect", cotion, promesse.getCotion());
	}
	
	@Test
	public void testCommission() {
		int com = (int)(3500*7/100);
		assertEquals("La commission est incorrect",com, promesse.getCommission());
	}
	
	@Test
	public void testPrixVendeur() {
		assertEquals("le prix est incorrect", 3500, promesse.getPrixVendeur());
	}
	
	@Test
	public void testAddrNotaire() {
		assertEquals("L'adresse du notaire est incorrect", "Springfield", promesse.getAddrNotaire());
	}
	
	@Test
	public void testDateVente() {
		assertEquals("La date est incorrect", "2019/02/09", promesse.getDateVente());
	}
	
	@Test
	public void testFrais() {
		assertEquals("Le frais est incorrect", 1000, promesse.getFrais());
	}
	
	@Test
	public void testBien() {
		assertEquals("Le bien est incorrect", maison, promesse.getBien());
	}
}

