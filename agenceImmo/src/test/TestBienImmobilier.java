package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import agenceImmo.Appart;
import agenceImmo.BienImmobilier;
import agenceImmo.Maison;
import agenceImmo.Mandat;
import agenceImmo.Personne;
import agenceImmo.Physique;
import agenceImmo.Terrain;

public class TestBienImmobilier {
	BienImmobilier maison;
	BienImmobilier appart;
	BienImmobilier terrain;
	Mandat mandat;
	Personne personne;
	@Before
	public void setUp() throws Exception {
		personne = new Physique("Homer","Springfield", "118218", "homerjsimpson@mail.com");
		Mandat mandat = new Mandat("2019/02/09", 3500, "1", personne);
		
		maison = new Maison(personne, 3500, "S", "Springfield", "2019/02/09", "1", 50, 5, 2, "Electrique");
		appart = new Appart(personne, 3500, "S", "Springfield", "2019/02/09", "1", 5, 2, 350);
		terrain = new Terrain(personne, 3500, "S", "Springfield", "2019/02/09", "1", 50, 100);
	}

	@Test
	public void testBien() {
		assertNotNull("Linstance n'est pas cr�e", maison);
		assertNotNull("Linstance n'est pas cr�e", appart);
		assertNotNull("Linstance n'est pas cr�e", terrain);
	}
	
	@Test
	public void testPersonne() {
		assertEquals("La personne est incorrect", personne, maison.getPersonne());
		assertEquals("La personne est incorrect", personne, appart.getPersonne());
		assertEquals("La personne est incorrect", personne, terrain.getPersonne());
		
	}
	
	@Test
	public void testPrix() {
		assertEquals("Le prix est incorrect", 3500, maison.getPrix());
		assertEquals("Le prix est incorrect", 3500, appart.getPrix());
		assertEquals("Le prix est incorrect", 3500, terrain.getPrix());
		
				
	}

}
