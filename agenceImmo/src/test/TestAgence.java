package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import agenceImmo.Agence;
import agenceImmo.Annonce;
import agenceImmo.BienImmobilier;
import agenceImmo.Maison;
import agenceImmo.Mandat;
import agenceImmo.Media;
import agenceImmo.Personne;
import agenceImmo.Physique;
import agenceImmo.Promesse;
import agenceImmo.RendezVous;

public class TestAgence {
	 ArrayList<Annonce> uneAnnonce;
	 ArrayList<BienImmobilier> bien1;
	 ArrayList<BienImmobilier> bienVendu;
	 ArrayList<RendezVous> rdv;
	 ArrayList<Personne> clients;
	 ArrayList<Promesse> promesse;
	
	Agence agence;
	Physique personne;
	@Before
	public void setUp() throws Exception {
		uneAnnonce = new ArrayList<Annonce>();
		bien1 = new ArrayList<BienImmobilier>();
		rdv = new ArrayList<RendezVous>();
		clients = new ArrayList<Personne>();
		promesse = new ArrayList<Promesse>();
		bienVendu = new ArrayList<BienImmobilier>();
		
		personne = new Physique("Homer","Springfield", "118218", "homerjsimpson@mail.com");
		agence = new Agence();
	}

	@Test
	public void testAgence() {
		
		assertNotNull("Linstance n'est pas cr�e", agence);
	}
	
	@Test
	public void testNewClient() {
		clients.add(personne);
		for(Personne p: agence.getClients()) {
			assertEquals("L'ajout d'un client est incorrect", personne, p);
		}
		
	}
	
	@Test
	public void testNewRdv() {
		Physique acheteur = new Physique("Homer","Springfield", "118218", "homerjsimpson@mail.com");
		Physique vendeur = new Physique("Ned","Springfield", "118218", "nedflanders@mail.com");
		
		RendezVous unRdv = new RendezVous("2019/02/09",vendeur,acheteur);
		rdv.add(unRdv);
		for(RendezVous r: agence.getRdv()) {
			assertEquals("L'ajout d'un client est incorrect", unRdv, r);
		}
	}
	
	@Test
	public void testNewRDVvente() {
		Physique acheteur = new Physique("Homer","Springfield", "118218", "homerjsimpson@mail.com");
		Physique vendeur = new Physique("Ned","Springfield", "118218", "nedflanders@mail.com");
		
		RendezVous unRdv = new RendezVous("2019/02/09",vendeur,acheteur);
		rdv.add(unRdv);
		
		for(RendezVous r: agence.getRdv()) {
			assertEquals("L'ajout d'un client est incorrect", unRdv, r);
		}
		
	}
	@Test
	public void testNewAchat() {
		Physique acheteur = new Physique("Homer","Springfield", "118218", "homerjsimpson@mail.com");
		Physique vendeur = new Physique("Ned","Springfield", "118218", "nedflanders@mail.com");
		
		Mandat mandat = new Mandat("2019/02/09", 3500, "1", vendeur);
		Maison maison = new Maison(vendeur, 3500, "S", "Springfield", "2019/02/09", "1", 50, 5, 2, "Electrique");
		
		RendezVous unRdv = new RendezVous("2019/02/09",vendeur,acheteur);
		Promesse pro = new Promesse(3500, "Springfield", "2019/02/09", 1000, maison);
		
		promesse.add(pro);
		for(Promesse p: agence.getPromesse()) {
			assertEquals("L'ajout d'une Promesse est incorrect", pro, p);
		}
		bienVendu.add(maison);
		for(BienImmobilier b: agence.getBien1()) {
			assertEquals("La maison est incorrect", maison, b);
		}
		bien1.add(maison);
		bien1.remove(maison);
		
		for(BienImmobilier b: agence.getBien1()) {
			assertNotEquals("La supression d'un bien est incorrect", maison, b);
		}
		
		
	}
	@Test
	public void testAddBien() {
		Physique vendeur = new Physique("Ned","Springfield", "118218", "nedflanders@mail.com");
		Maison maison = new Maison(vendeur, 3500, "S", "Springfield", "2019/02/09", "1", 50, 5, 2, "Electrique");
		bien1.add(maison);
		
		for(BienImmobilier b: agence.getBien1()) {
			assertEquals("La maison est incorrect", maison, b);
		}
	}
	
	@Test
	public void testNewPub() {
		Physique vendeur = new Physique("Ned","Springfield", "118218", "nedflanders@mail.com");
		Maison maison = new Maison(vendeur, 3500, "S", "Springfield", "2019/02/09", "1", 50, 5, 2, "Electrique");
		Media media1 = null;
		media1 = Media.internet;
		Annonce annonce = new Annonce(media1, maison);
		this.uneAnnonce.add(annonce);
		
		for(Annonce a: agence.getUneAnnonce()) {
			assertEquals("L'annonce est incorrect", annonce, a);
		}
	}
	
	@Test
	public void testVenteRealise() {
		Physique vendeur = new Physique("Ned","Springfield", "118218", "nedflanders@mail.com");
		Maison maison = new Maison(vendeur, 3500, "S", "Springfield", "2019/02/09", "1", 50, 5, 2, "Electrique");
		
		this.bien1.remove(maison);
		for(BienImmobilier b: agence.getBien1()) {
			assertNotEquals("La supression d'un bien est incorrect", maison, b);
		}
		this.bienVendu.add(maison);
		for(BienImmobilier b: agence.getBienVendu()) {
			assertNotEquals("La supression d'un bien est incorrect", maison, b);
		}
	}
	
	
	

}
