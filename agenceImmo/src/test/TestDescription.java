package test;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import agenceImmo.BienImmobilier;
import agenceImmo.Description;
import agenceImmo.Maison;
import agenceImmo.Personne;
import agenceImmo.Physique;
import agenceImmo.Media;

public class TestDescription {
	BienImmobilier maison;
	Personne personne;
	
	Description uneDescription;
	
	@Before
	public void setUp() throws Exception {
		Media med = null;
		med = Media.internet;
		personne = new Physique("Homer","Springfield", "118218", "homerjsimpson@mail.com");
		maison = new Maison(personne, 3500, "S", "Springfield", "2019/02/09", "1", 50, 5, 2, "Electrique");
		uneDescription = new Description(med, maison);
	}

	@Test
	public void testMedia() {
		assertNotNull("Linstance n'est pas cr�e", uneDescription);
	}

}
