package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import agenceImmo.Voeux;

public class TestVoeux {
	Voeux voeux;

	@Before
	public void setUp() throws Exception {
		voeux = new Voeux("t1","Springfield",3500,25,5);
	}

	@Test
	public void testVoeux() {
		assertNotNull("Linstance n'est pas cr�e", voeux);
		
	}
	@Test
	public void testPiece() {
		assertEquals("La piece est incorrect", 5, voeux.getPiece());

	}
	
	@Test
	public void testType() {
		assertEquals("Le type est incorrect", "t1", voeux.getType());
	}

	@Test
	public void testLocalisation() {
		assertEquals("La localisation est incorrect", "Springfield", voeux.getLocalisation());
	}

	@Test
	public void testPrix() {
		assertEquals("Le prix est incorrect", 3500, voeux.getPrix());
	}

	@Test
	public void testSurface() {
		assertEquals("La surface est incorrect", 25, voeux.getSurface());
	}
}
