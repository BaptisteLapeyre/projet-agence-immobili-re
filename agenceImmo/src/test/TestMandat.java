package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import agenceImmo.Mandat;
import agenceImmo.Personne;
import agenceImmo.Physique;

public class TestMandat {
	Mandat mandat;
	Personne personne;
	
	
	@Before
	public void setUp() throws Exception {
		personne = new Physique("Homer","Springfield", "118218", "homerjsimpson@mail.com");
		mandat = new Mandat("2019/02/09", 3500, "1", personne);
	}

	@Test
	public void testMandat() {
		assertNotNull("Linstance n'est pas cr�e", mandat);
	}

	@Test
	public void testPrix() {
		assertEquals("Le prix est incorrect", 3500, mandat.getPrix());
	}
	
	@Test
	public void testDuree() {
		assertEquals("La duree est incorrect", "1", mandat.getDuree());
	}
	
	@Test
	public void testPersonne() {
		assertEquals("La personne est incorrect", personne, mandat.getPersonne());
	}
	
	@Test
	public void testDate() {
		assertEquals("La date  est incorrect", "2019/02/09", mandat.getDateSouhaitee());
	}
}
