package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import agenceImmo.BienImmobilier;
import agenceImmo.Maison;
import agenceImmo.Mandat;
import agenceImmo.Personne;
import agenceImmo.Physique;
import agenceImmo.RendezVous;

public class TestRendezVous {

	 Physique acheteur;
	 Physique vendeur;
	 Maison maison;
	 Mandat mandat;
	 
	 RendezVous rdv;

	@Before
	public void setUp() throws Exception {
		acheteur = new Physique("Homer","Springfield", "118218", "homerjsimpson@mail.com");
		vendeur = new Physique("Ned","Springfield", "118218", "nedflanders@mail.com");
		
		mandat = new Mandat("2019/02/09", 3500, "1", vendeur);
		maison = new Maison(vendeur, 3500, "S", "Springfield", "2019/02/09", "1", 50, 5, 2, "Electrique");
		
		rdv = new RendezVous("2019/02/09",vendeur,acheteur);
	}

	@Test
	public void testRdv() {
		assertNotNull("Linstance n'est pas cr�e", rdv);
	}
	
	public void testDate() {
		assertEquals("La date est incorrect", "2019/02/09", rdv.getDate());
	}

	public void testVendeur() {
		assertEquals("Le vendeur est incorrect", vendeur, rdv.getVendeur());
	}

	public void testAcheteur() {
		assertEquals("L'acheteur est incorrect", acheteur, rdv.getAcheteur());
	}
	
	public void testMandat() {
		assertEquals("Le mandat est incorrect", mandat, rdv.getMandat());
	}
	
	public void testBien() {
		assertEquals("Le bien est incorrect", maison, rdv.newBien(BienImmobilier.Type.MAISON, 0, vendeur, 3500, "S", "Springfield", "2019/02/09", "1", 50, 5, 2, 0, "Electrique", 0, 0));
		
	}
	
	
}

